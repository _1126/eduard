#! /bin/sh
#|  -*- mode: scheme -*-
exec csi -s "$0" "$@"
|#


(use sql-de-lite ssql
     files uuid-lib posix
     srfi-1)

(import matchable)


;; Creating the database
(define base-path (make-pathname (get-environment-variable "HOME") ".eduard"))
(define database-name "eduard3.sqlite")
(create-directory base-path #t)

(define create-database-string "CREATE TABLE transactions (uuid TEXT NOT NULL PRIMARY KEY, amount INT NOT NULL, currency TEXT, description TEXT NOT NULL, timestamp INT NOT NULL, refersTo TEXT)")

(define database
  (open-database (make-pathname base-path database-name)))

(define (ensure-database-exists!)
  (when (null? (exec (sql database
                          (ssql->sql #f `(select (columns name)
                                           (from sqlite_master)
                                           (where (and (= type "table")
                                                       (= name "transactions"))))))))
    (exec (prepare database create-database-string))))

(ensure-database-exists!)


;; Internal helper functions
(define (amount->string amount)
  (sprintf "~A€" (/ amount 100)))

(define (timestamp->datestring timestamp)
  (time->string (seconds->local-time timestamp) "%d.%m.%Y %H:%M"))

(define (print-transaction amount description timestamp)
  (printf "Amount:      \t~A\n" (amount->string amount))
  (printf "Description: \t~A\n" description)
  (printf "Date:        \t~A\n\n" (timestamp->datestring timestamp)))


;; Insert new transactions into database
(define (insert-statement database)
  (sql database (ssql->sql #f `(insert (into transactions)
                                       (columns uuid amount currency description timestamp refersTo)
                                       (values #(? ? ? ? ? ?))))))

(define (insert amount description refersTo)
  (exec (insert-statement database) (uuid->string (make-uuid 'V4)) amount "euro" description (current-seconds) refersTo))


;; Query the database
(define query-statement-current-status
  (ssql->sql #f `(select (columns (call sum amount))
                   (from transactions))))

(define query-statement-latest-transaction
  (ssql->sql #f `(select (columns uuid amount description timestamp)
                   (from transactions)
                   (where (= timestamp (select (columns (call max timestamp))
                                         (from transactions)))))))

(define query-statement-all-transactions
  (ssql->sql #f `(select (columns amount description timestamp)
                   (from transactions)
                   (order (asc timestamp)))))

(define query-statement-total-spent
  (ssql->sql #f `(select (columns (call sum amount))
                   (from transactions)
                   (where (< amount 0)))))

(define query-statement-total-deposited
  (ssql->sql #f `(select (columns (call sum amount))
                   (from transactions)
                   (where (> amount 0)))))

(define (prepare-query query-statement)
  (sql database query-statement))

(define latest-transaction-data
  (exec (prepare-query query-statement-latest-transaction)))

(define all-transactions-data
  (query fetch-all (prepare-query query-statement-all-transactions)))

(define total-spent-data
  (exec (prepare-query query-statement-total-spent)))

(define total-deposited-data
  (exec (prepare-query query-statement-total-deposited)))

(define (query specification)
  (when (null? latest-transaction-data)
    (begin (print "No transactions done so far.")
           (exit 0)))
  (match specification
    ("current-status"
     (begin (printf "Current amount:\t~A\n"
                    (amount->string (first (exec (prepare-query query-statement-current-status)))))
            (printf "Last changed:  \t~A\n"
                    (timestamp->datestring (fourth latest-transaction-data)))))
    ("latest-transaction"
     (print-transaction (second latest-transaction-data) (third latest-transaction-data) (fourth latest-transaction-data)))
    ("all-transactions"
     (begin (for-each (lambda (n) (print-transaction (first n) (second n) (third n))) all-transactions-data)
            (printf "Transactions done so far: ~A\n" (length all-transactions-data))))
    ("total-spent"
     (if (null? (car total-spent-data))
         (print "Nothing spent so far.")
         (printf "Total amount spent: ~A\n" (amount->string (car total-spent-data)))))
    ("total-deposited"
     (if (null? (car total-deposited-data))
         (print "Nothing deposited so far.")
         (printf "Total amount deposited: ~A\n" (amount->string (car total-deposited-data)))))
    (else (print "What did you ask?"))))


;; Usage and help information
(define (usage #!optional exit-code)
  (let ((port (if (and exit-code (not (zero? exit-code)))
                  (current-error-port)
                  (current-output-port))))
    (fprintf port "eduard is a tool to keep record of your finances.\n\n")

    (fprintf port "Usage:\n\n")
    (fprintf port "\teduard command <arguments>\n\n")

    (fprintf port "The available commands are:\n\n")
    (fprintf port "\ttransact      \ttransacts an order\n")
    (fprintf port "\tquery         \tqueries the database for various information\n")
    (fprintf port "\trevert-latest \treverts the latest transaction\n\n")

    (fprintf port "Use \"eduard help <command>\" to get more information about a command.\n")))

(define (help command)
  (match command
    ("transact"
     (print "usage: eduard transact <amount> <description>\n")
     (print "'transact' deposits or disburses money from the account,")
     (print "depending on whether the <amount> is positive or negative.")
     (print "<amount> is always in euro cents.\n")
     (print "The <description> has to be a enclosed with \"\".\n")
     (print "Example: eduard transact -1309 \"grocery shopping\""))
    ("query"
     (print "usage: eduard query <argument>\n")
     (print "'query' gives you information about your finances.\n")

     (print "Available arguments:\n")
     (print "current-status    \t information about the current financial status")
     (print "latest-transaction\t information about the latest transaction")
     (print "all-transactions  \t prints out all transactions done so far")
     (print "total-spent       \t prints out the sum of the amount disbursed")
     (print "total-deposited   \t prints out the sum of the amount deposited"))
    (else (print "eduard does not know what to with '" command "'"))))


;; Match the arguments from the command line
(match (command-line-arguments)
  (()
   (usage 0))
  (("help")
   (usage 1))
  (("help" command)
   (help command))
  (("transact")
   (print "How should eduard transact le néant?"))
  (("transact" (-?> string->number amount) description)
   (insert amount description ""))
  (("query")
   (print "eduard does not know what you've asked him."))
  (("query" specification)
   (query specification))
  (("revert-latest") 
   (insert (* (second latest-transaction-data) -1) "reverting latest transaction" (sprintf "~A"  (first latest-transaction-data))))
  (else
   (print "eduard does not know what to with '" (command-line-arguments) "'")))

(close-database database)
